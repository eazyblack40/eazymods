@echo off

:: BatchGotAdmin
:-------------------------------------
REM  --> Check for permissions
    IF "%PROCESSOR_ARCHITECTURE%" EQU "amd64" (
>nul 2>&1 "%SYSTEMROOT%\SysWOW64\cacls.exe" "%SYSTEMROOT%\SysWOW64\config\system"
) ELSE (
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"
)

REM --> If error flag set, we do not have admin.
if '%errorlevel%' NEQ '0' (
    echo Requesting administrative privileges...
    goto UACPrompt
) else ( goto gotAdmin )

:UACPrompt
    echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
    set params= %*
    echo UAC.ShellExecute "cmd.exe", "/c ""%~s0"" %params:"=""%", "", "runas", 1 >> "%temp%\getadmin.vbs"

    "%temp%\getadmin.vbs"
    del "%temp%\getadmin.vbs"
    exit /B

:gotAdmin
    pushd "%CD%"
    CD /D "%~dp0"
:--------------------------------------
echo Welcome
title Hello
echo Remove Or Add Fake Domains?
echo 1 = Add
echo 2 = Remove
choice /c 12
IF ERRORLEVEL 2 GOTO remove
IF ERRORLEVEL 1 GOTO add
:add
echo WARMING Before Continuing You Need Python or NodeJS
echo If You Have Python Select 1, If you have NodeJS Select 2 , If you have both select one of two
echo You need to have them added to PATH
echo This will also take port 80, so if you have any web servers shut them down
choice /c 12
IF ERRORLEVEL 2 GOTO nodejs
IF ERRORLEVEL 1 GOTO python
:nodejs
echo Type What Domain You need to add to the hosts file
echo WARMING : That OVERWRITES DNS So Be Careful to not add any domain that its needed like google.com
echo You Can Restore everything you do by choosing remove
echo Domains with spaces will not work
set /p domain=
echo Adding
echo 127.0.0.1  %domain% >> %windir%\System32\drivers\etc\hosts
mkdir %userprofile%\Desktop\fakedomains
cd /d %userprofile%\Desktop\fakedomains
echo Open README.txt to see how to config it > index.html
echo Edit Index.html and add any website code you want > README.txt
echo Open The start.bat File to Launch the fake domain server >> README.txt
echo To remove this open the installer and select remove >> README.txt
echo @echo off > start.bat
echo echo Starting >> start.bat
echo npx http-server -p 80 -d false >> start.bat
echo Done Go to Desktop and open fakedomains folder
goto exit
:python
echo python part isnt ready sorry
goto exit
:remove
echo Press any key to remove
pause >nul
del %windir%\System32\drivers\etc\hosts
echo # Copyright (c) 1993-2009 Microsoft Corp. >> %windir%\System32\drivers\etc\hosts
echo # >> %windir%\System32\drivers\etc\hosts
echo # This is a sample HOSTS file used by Microsoft TCP/IP for Windows. >> %windir%\System32\drivers\etc\hosts
echo # >> %windir%\System32\drivers\etc\hosts
echo # This file contains the mappings of IP addresses to host names. Each >> %windir%\System32\drivers\etc\hosts
echo # entry should be kept on an individual line. The IP address should >> %windir%\System32\drivers\etc\hosts
echo # be placed in the first column followed by the corresponding host name. >> %windir%\System32\drivers\etc\hosts
echo # The IP address and the host name should be separated by at least one >> %windir%\System32\drivers\etc\hosts
echo # space. >> %windir%\System32\drivers\etc\hosts
echo # >> %windir%\System32\drivers\etc\hosts
echo # Additionally, comments (such as these) may be inserted on individual >> %windir%\System32\drivers\etc\hosts
echo # lines or following the machine name denoted by a '#' symbol. >> %windir%\System32\drivers\etc\hosts
echo # >> %windir%\System32\drivers\etc\hosts
echo # For example: >> %windir%\System32\drivers\etc\hosts
echo # >> %windir%\System32\drivers\etc\hosts
echo #      102.54.94.97     rhino.acme.com          # source server >> %windir%\System32\drivers\etc\hosts
echo #       38.25.63.10     x.acme.com              # x client host >> %windir%\System32\drivers\etc\hosts
echo # >> %windir%\System32\drivers\etc\hosts
echo # localhost name resolution is handled within DNS itself. >> %windir%\System32\drivers\etc\hosts
echo #	127.0.0.1       localhost >> %windir%\System32\drivers\etc\hosts
echo #	::1             localhost >> %windir%\System32\drivers\etc\hosts
rmdir /S /Q %userprofile%\Desktop\fakedomains
echo Succesfully Removed All Fake Domains
goto exit
:exit
echo Thanks for using it
echo by Eazyblack
echo Press any key to exit
pause >nul
